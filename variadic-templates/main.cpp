#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <memory>

using namespace std;

void print()
{}

template <typename T, typename... Rest>
void print(const T& first, const Rest&... rest)
{
    cout << first << " ";
    print(rest...);
}

template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
    return unique_ptr<T>{new T(forward<Args>(args)...)};
}

class Gadget
{
    int id_ = 0;
    string name_{"unknown"};
public:
    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(id: " << id_ << ")" << endl;
    }

    explicit Gadget(int id) : id_{id}
    {
        cout << "ctor Gadget(id: " << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void close()
    {
        cout << "Gadget is off" << endl;
    }

    virtual void play() const
    {
        cout << "Gadget::play()" << endl;
    }

    bool operator==(const Gadget& other) const
    {
        return tie(id_, name_) == tie(other.id_, other.name_);
    }

    bool operator<(const Gadget& other) const
    {
        return tie(id_, name_) < tie(other.id_, other.name_);
    }
};


int main()
{
    print(5, 15, "text", 3.14);

    auto g1 = my_make_unique<Gadget>(6);
    auto g2 = my_make_unique<Gadget>();

}
