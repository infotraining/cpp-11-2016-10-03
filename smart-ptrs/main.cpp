#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <memory>
#include <tuple>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

class Gadget
{
    int id_ = 0;
    string name_{"unknown"};
public:
    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(id: " << id_ << ")" << endl;
    }

    explicit Gadget(int id) : id_{id}
    {
        cout << "ctor Gadget(id: " << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void close()
    {
        cout << "Gadget is off" << endl;
    }

    virtual void play() const
    {
        cout << "Gadget::play()" << endl;
    }

    bool operator==(const Gadget& other) const
    {
        return tie(id_, name_) == tie(other.id_, other.name_);
    }

    bool operator<(const Gadget& other) const
    {
        return tie(id_, name_) < tie(other.id_, other.name_);
    }
};

unique_ptr<Gadget> gadget_factory()
{
    static int seed = 0;

    return unique_ptr<Gadget>{new Gadget(++seed)};
}

void use(unique_ptr<Gadget> g)
{
    g->play();
}

void use(Gadget& g)
{
    g.play();
}

namespace legacy
{
    void use(Gadget* g)
    {
        g->play();
    }
}

TEST_CASE("unique_ptr")
{
    SECTION("default ctor sets pointer to 0")
    {
        unique_ptr<Gadget> ptr;

        REQUIRE(ptr == nullptr);
        REQUIRE(ptr.get() == nullptr);
    }

    SECTION("control lifetime of dynamic object")
    {
        unique_ptr<Gadget> ptr_g1{new Gadget{1}};  // C++11
        unique_ptr<Gadget> ptr_g2 = make_unique<Gadget>(2); // C++14

        ptr_g1->play();
        ptr_g2->play();
    }

    SECTION("is only moveable")
    {
        unique_ptr<Gadget> ptr_g1{new Gadget{1}};
        // unique_ptr<Gadget> ptr_g2 = ptr_g1;  // compilation error

        unique_ptr<Gadget> ptr_g2 = move(ptr_g1);

        REQUIRE(ptr_g1 == nullptr);
        REQUIRE(ptr_g2.get() != nullptr);

        ptr_g2->play();
    }

    SECTION("can be returned from function")
    {
        unique_ptr<Gadget> ptr_g1 = gadget_factory();
        ptr_g1->play();
    }

    SECTION("can be used as sink argument")
    {
        unique_ptr<Gadget> ptr_g1{new Gadget(1)};
        legacy::use(ptr_g1.get());
        use(*ptr_g1); // only use
        use(move(ptr_g1)); // use & destroy

        use(gadget_factory());
    }

    SECTION("can be stored in container")
    {
        vector<unique_ptr<Gadget>> gadgets;

        unique_ptr<Gadget> ptr_g1{new Gadget(1)};

        gadgets.push_back(move(ptr_g1));
        gadgets.push_back(unique_ptr<Gadget>(new Gadget{2}));
        gadgets.push_back(gadget_factory());

        for(const auto& g : gadgets)
            g->play();
    }

    SECTION("can manage dynamic arrays")
    {
//        int* tab = new int[100];
//        delete [] tab;

        unique_ptr<Gadget[]> dynamic_array{new Gadget[100]};
        dynamic_array[33] = Gadget{77};
    }

    SECTION("has support for custom deallocator")
    {
        struct GadgetCloser
        {
            void operator()(Gadget* g)
            {
                g->close();
            }
        };

        Gadget g{665};
        unique_ptr<Gadget, GadgetCloser> close_guard{&g};

        g.play();
    }

    SECTION("has support for custom deallocator as lambda")
    {
        auto gadget_closer = [](Gadget* g) { g->close(); };

        Gadget g{665};
        unique_ptr<Gadget, decltype(gadget_closer)> close_guard{&g, gadget_closer};

        g.play();
    }
}
