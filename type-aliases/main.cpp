#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <map>
#include <array>
#include <type_traits>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

template <typename Value>
using StringDict = map<string, Value>;

template <size_t N>
using Array = array<int, N>;

template <typename Value>
using StringDict = map<string, Value>;

TEST_CASE("using declaration")
{
    SECTION("replaces typedef")
    {
        using IdType = int; // typedef int IdType;
    }

    SECTION("template alias can be parametrized with a type")
    {
        StringDict<int> dict = { {"one", 1}, {"two", 2} };
    }

    SECTION("can be parametrized with a integral constant")
    {
        Array<10> arr1; // array<int, 10>
        Array<20> arr2;

    }

    SECTION("C++14 uses alises for type_traits")
    {
        enum Coffee : u_int8_t { espresso, latte, cappucino };

        //typename underlying_type<Coffee>::type value; // C++11
        underlying_type_t<Coffee> value;
    }
}

void legacy_code(int* array, size_t size)
{
    for(int* it = array; it != array + size; ++it)
        cout << *it << " ";
    cout << endl;
}

TEST_CASE("array")
{
    SECTION("replaces native tabs in C++11")
    {
        int tab[10] = { 1, 2, 3, 4 };

        array<int, 10> arr = { 1, 2, 3, 4 };

        REQUIRE(arr.size() == 10);
        REQUIRE(arr[3] == 4);

        legacy_code(arr.data(), arr.size());
    }
}
