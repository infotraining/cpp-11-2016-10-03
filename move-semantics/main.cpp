#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <memory>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

class Gadget
{
    int id_ = 0;
    string name_{"unknown"};
public:
    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(id: " << id_ << ")" << endl;
    }

    explicit Gadget(int id) : id_{id}
    {
        cout << "ctor Gadget(id: " << id_ << ")" << endl;
    }

    Gadget(const Gadget& source)
    {
        cout << "Gadget(cc id: " << source.id() << ")\n";
        id_ = source.id();
        name_ = source.name();
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    virtual void play() const
    {
        cout << "Gadget::play()" << endl;
    }
};


namespace legacy
{
    void use_gadget(auto_ptr<Gadget> g)
    {
        g->play();
    }
}

void use_gadget(unique_ptr<Gadget> g)
{
    g->play();
}

int foo()
{
    return 42;
}

void use(Gadget& g)
{
    cout << "using(&):  ";
    g.play();
}

void use(const Gadget& g)
{
    cout << "using(const&): ";
    g.play();
}

void use(Gadget&& g)
{
    cout << "using(&&): ";
    g.play();
}

class SmartObject
{
    Gadget* g_;
    string guid_;
public:
    SmartObject(int id, const string& guid)
        : g_{new Gadget(id)}, guid_{guid}
    {
        cout << "SmartObject(id: " << id << ")\n";
    }

    SmartObject(const SmartObject& source)
        : g_{new Gadget(*(source.g_))}, guid_{source.guid_}
    {
        cout << "SmartObject(cc id: " << g_->id() << ")\n";
    }

    SmartObject& operator=(const SmartObject& source)
    {
        cout << "SmartObject=(cc id: " << source.g_->id() << ")\n";
        if (this != &source)
        {
            Gadget* temp = new Gadget(*(source.g_));
            guid_ = source.guid_;
            delete g_;
            g_ = temp;
        }

        return *this;
    }

    // move sematics impl
    SmartObject(SmartObject&& source) noexcept
        : g_{source.g_}, guid_{move(source.guid_)}
    {
        cout << "SmartObject(mv id: " << g_->id() << ")\n";
        source.g_ = nullptr;
    }

    SmartObject& operator=(SmartObject&& source) noexcept
    {
        cout << "SmartObject=(mv id: " << source.g_->id() << ")\n";

        if (this != &source)
        {
            g_ = source.g_;
            source.g_ = nullptr;
            guid_ = move(source.guid_);
        }

        return *this;
    }


    ~SmartObject()
    {
        if (g_)
            cout << "~SmartObject(id: " << g_->id() << ")\n";

        delete g_;
    }
};


class HyperObject
{
    SmartObject so_;
public:
    HyperObject(int id, const string& guid) : so_{id, guid}
    {
        cout << "HyperObject()" << endl;
    }

    HyperObject(const HyperObject&) = default;
    HyperObject& operator=(const HyperObject&) = default;

    HyperObject(HyperObject&&) = default;
    HyperObject& operator=(HyperObject&&) = default;

    ~HyperObject()
    {
        cout << "~HyperObject()" << endl;
    }
};

TEST_CASE("Binding refs")
{
    int x = 10;
    int& lref1 = x;
    // int& lref2 = foo(); // error
    const int& clref1 = foo(); // ok

    // C++11
    int&& rref1 = foo();
    //int&& rref2 = x; // error

    Gadget g1{1};
    const Gadget g2{2};

    use(g1);
    use(g2);
    use(Gadget{3});
}

class Object
{
public:
    Object(int)
    {
        cout << "Object(int)" << endl;
    }

    Object(const string&)
    {
        cout << "Object(const string&)" << endl;
    }

    Object(string&&)
    {
        cout << "Object(string&&)" << endl;
    }
};

class ObjectWrapper
{
    Object o_;
public:
    template <typename Arg>
    ObjectWrapper(Arg&& arg) : o_{forward<Arg>(arg)}
    {
    }
};

TEST_CASE("SmartObject with move semantics")
{
    cout << "-------\n\n\n";

    vector<SmartObject> vec;

    vec.push_back(SmartObject(1, "g1"));
    vec.push_back(SmartObject(2, "g2"));
    vec.push_back(SmartObject(3, "g3"));
    vec.push_back(SmartObject(4, "g4"));

    cout << "-------\n\n\n";

    vector<HyperObject> coll;

    coll.push_back(HyperObject{3, "g3"});
    coll.push_back(HyperObject{4, "g4"});
    coll.push_back(HyperObject{5, "g5"});
}


TEST_CASE("auto_ptr problem")
{
//    using namespace legacy;

//    auto_ptr<Gadget> g1(new Gadget(1));
//    g1->play();

//    use_gadget(g1);

//    auto_ptr<Gadget> g2 = g1;
//    g2->play();

//    REQUIRE(g1.get() == nullptr);

    SECTION("unique_ptr solution")
    {
        cout << "\n\n";

        unique_ptr<Gadget> g1(new Gadget(1));
        g1->play();

        use_gadget(move(g1));
    }
}

TEST_CASE("move doesn't move")
{
    cout << "\n\n";

    const HyperObject ho{1, "ho1"};

    HyperObject ho_target = move(ho);
}

TEST_CASE("perfect forwarding")
{
    ObjectWrapper ow1(1);

    string str = "text";
    ObjectWrapper ow2{str};

    ObjectWrapper ow3{string("temp")};
}
