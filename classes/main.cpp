#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <array>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

class Gadget
{
    int id_ = 0;
    string name_{"unknown"};
    array<int, 2> dimensions_ = { {0, 0} };  // C++17 - mustache collapsing
public:
    Gadget() = default;
    virtual ~Gadget() = default;

    Gadget(int id) : Gadget(id, "not-set", -1, -1) // ctor delegation
    {
        name_ = "unknown";
        cout << "ctor Gadget(id: " << id_ << ")" << endl;
    }

    Gadget(int id, const string& name, int w, int h)
        : id_{id}, name_{name}, dimensions_{w, h}
    {
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    virtual void play() const
    {
        cout << "Gadget::play()" << endl;
    }

    const array<int, 2>& dimensions() const
    {
        return dimensions_;
    }
};

class ExtraGadget: public Gadget
{
public:
    using Gadget::Gadget; // all ctors from base class are inherited

    ExtraGadget() = default;

    ExtraGadget(int id) : Gadget{id}
    {
        cout << "ctor ExtraGadget(id: " << id << ")" << endl;
    }

    void play() const final override
    {
        cout << "ExtraGadget::play()s better" << endl;
    }
};

class NoCopyable
{
public:
    NoCopyable() = default;
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

void calc(int x)
{
    cout << "calc(int)" << endl;
}

void calc(double) = delete;

class Array
{
public:
    int items_[10];

    int& operator[](size_t index)
    {
        return items_[index];
    }
};

TEST_CASE("defaulted special functions")
{
    Gadget g0;

    REQUIRE(g0.id() == 0);
    REQUIRE(g0.name() == "unknown");

    Gadget g1{1};
    REQUIRE(g1.name() == "unknown");

    Gadget g2{2, "ipad", 100, 200};
    REQUIRE(g2.id() == 2);
    REQUIRE(g2.name() == "ipad");
    REQUIRE(g2.dimensions()[0] == 100);
    REQUIRE(g2.dimensions()[1] == 200);

}

TEST_CASE("deleted functions")
{
    NoCopyable nc;
    // NoCopyable cpy_nc = nc; // compilation error

    calc(42);

    short x = 22;
    calc(x);

    //calc(3.14);  // compilation error - use of deleted func void calc(double)
}

TEST_CASE("constructor inheritance")
{
    ExtraGadget eg0;
    eg0.play();

    ExtraGadget eg1{1};
    Gadget& ref_g = eg1;
    ref_g.play();
}

TEST_CASE("problem with array init")
{
    int tab[10] = { 1, 2, 3, 4 };
    array<int, 10> arr = { 1, 2, 3, 4 };
}
