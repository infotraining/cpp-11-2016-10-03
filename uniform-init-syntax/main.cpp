#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <map>
#include <algorithm>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

double get_value()
{
    return 3.14;
}

TEST_CASE("uniform init syntax")
{
    SECTION("for simple types")
    {
        int x{};
        REQUIRE(x == 0);

        int* ptr{};
        REQUIRE(ptr == nullptr);

        int y{5};
        REQUIRE(y == 5);

        bool state{true};
        REQUIRE(state);

        SECTION("narrowing converion is an error")
        {
            //int x{3.14};
            //int y{get_value()}; // warning

            int z{static_cast<int>(get_value())};
        }
    }

    SECTION("for native arrays")
    {
        int tab[10] = { 1, 2, 3, 4 };
    }

    SECTION("for aggregates")
    {
        struct Point
        {
            int x;
            int y;
        };

        Point pt1 = {1, 6};
        Point pt2{5, 6};
    }

    SECTION("for classes with constructor")
    {
        class Gadget
        {
            int id_;
            string name_;
        public:
            Gadget(int id, string name) : id_{id}, name_{name}
            {}

            int id() const
            {
                return id_;
            }

            string name() const
            {
                return name_;
            }

        };

        Gadget g1 {1, "mp3 player"}; // Gadget(1, "mp3 player")
        Gadget g2 = {2, "ipad"};
    }

    SECTION("for containers")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 };

        REQUIRE(vec.size() == 5);
        REQUIRE(vec.front() == 1);
        REQUIRE(vec.back() == 5);

        map<string, int> dict = { {"one", 1}, {"two", 2} };
        REQUIRE(dict.size() == 2);
        REQUIRE(dict["two"] == 2);
    }
}

TEST_CASE("initializer_list")
{
    SECTION("is defined with {}")
    {
        std::initializer_list<int> il = { 1, 2, 3, 4 };

        SECTION("has simplified container interface")
        {
            const int* it1 = il.begin();
            REQUIRE(*it1 == 1);

            const int* it2 = il.end();

            REQUIRE(il.size() == 4);
        }
    }

    SECTION("allows passing multiple items as an argument")
    {
        class Stack
        {
            std::vector<int> stack_;
        public:
            void push(int item)
            {
                stack_.push_back(item);
            }

            void push(std::initializer_list<int> items)
            {
                for(const auto& item : items)
                    stack_.push_back(item);
            }

            size_t size() const
            {
                return stack_.size();
            }
        };

        Stack s;

        s.push(1);
        s.push({2, 3, 4});

        REQUIRE(s.size() == 4);
    }

    SECTION("initializer_list in constructors caveat")
    {
        SECTION("constructor called with ()")
        {
            vector<int> vec(5, 10);

            REQUIRE(vec.size() == 5);
            REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == 10;}));
        }

        SECTION("constructor called with {}")
        {
            vector<int> vec{5, 10};

            REQUIRE(vec.size() == 2);
            REQUIRE(vec.front() == 5);
            REQUIRE(vec.back() == 10);

            vector<string> word{5, "five"};
        }
    }
}

TEST_CASE("auto with il")
{
    auto items1 = { 1, 2, 3, 4 };  // std::initializer_list<int>

    SECTION("change in C++ standard for {element}")
    {
        SECTION("C++11")
        {
            auto item_a{1};  // std::initializer_list
            auto item_b = { 1 }; // std::initializer_list
        }

        SECTION("C++17")
        {
            auto item_a{1};  // int
            auto item_b =  { 1 } ; // std::initializer_list
        }
    }
}

class Gadget
{
    int id_;
    string name_{"unknown"};
    vector<int> params_;
public:
    Gadget(int id) : id_{id}, params_{1, 2}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

TEST_CASE("Gadgets constructor call")
{
    Gadget g1(1);
    Gadget g2{2};
}

