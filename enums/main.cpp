#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <type_traits>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

TEST_CASE("enums")
{
    SECTION("forward declaration for enums")
    {
        enum Coffee : u_int8_t;
    }

    SECTION("can be defined on specific integer type")
    {
        enum Coffee : u_int8_t { espresso = 1, latte, cappucino };

        REQUIRE(sizeof(Coffee) == 1);

        SECTION("underlying type can be found")
        {
            underlying_type<Coffee>::type value;

            value = latte;

            REQUIRE(value == 2);
        }
    }
}

TEST_CASE("scoped enumerations")
{
    SECTION("forward declaration for enums")
    {
        enum class Coffee : u_int8_t;
    }

    enum class Coffee : u_int8_t { espresso = 1, latte, cappucino };

    SECTION("values are in scope")
    {
        Coffee coffee = Coffee::cappucino;
    }

    SECTION("implicit conversion to integer is forbidden")
    {
        // int value = Coffee::latte;  // compilation error

        SECTION("can be converted with static cast")
        {
            int value = static_cast<int>(Coffee::latte);

            REQUIRE(value == 2);
        }
    }

    SECTION("implicit conversion from integer is forbidden")
    {
        // Coffee coffee = 3; // compilation error

        SECTION("can be converted with static cast")
        {
            Coffee coffee = static_cast<Coffee>(3);

            REQUIRE(coffee == Coffee::cappucino);
        }
    }

    SECTION("implicit conversion from another scope enum is forbidden")
    {
        enum class Engine { diesel, gas };

        // Coffee coffee = Engine::diesel; compilation error
    }
}

