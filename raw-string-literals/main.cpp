#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str1 = "c:\name\to_read\backup";
    cout << str1 << endl;

    cout << "raw-string-literals\n\n";

    // raw-string
    string str2 = R"(c:\name\to_read\backup)";
    cout << str2 << endl;

    string str3 = R"baal(text"()"text)baal";
    cout << str3 << endl;

    string str4 = R"(Line1
Line2
Line3)";

    cout << str4 << endl;
}
