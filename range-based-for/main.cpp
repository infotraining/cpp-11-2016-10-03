#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <list>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

TEST_CASE("range-based-for")
{
    SECTION("allows iteration over container")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 };

        for(int item : vec)
            cout << item << " ";
        cout << endl;

        SECTION("how-it-works")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                int item = *it;
                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("allows iteration over native tab")
    {
        int tab[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        for(int item : tab)
            cout << item << " ";
        cout << endl;
    }

    SECTION("allows iteration over initializer-lists")
    {
        for(const auto& item : { 1, 2, 3 })
            cout << item << " ";
        cout << endl;
    }

    class MyContainer
    {
        list<int> items_{1, 2, 3};

    public:

        typedef list<int>::iterator iterator;
        using const_iterator = list<int>::const_iterator;

        iterator begin()
        {
            return items_.begin();
        }

        iterator end()
        {
            return items_.end();
        }

        const_iterator begin() const
        {
            return items_.begin();
        }

        const_iterator end() const
        {
            return items_.end();
        }
    };

    SECTION("allows iteration over custom containers")
    {
        MyContainer my_container;

        for(const auto& item : my_container)
            cout << item << " ";
        cout << endl;
    }

    SECTION("can modifiy items in container")
    {
        vector<int> numbers = { 1, 2, 3 };

        for(int& item : numbers)
            item++;

        auto expected = { 2, 3, 4 };

        REQUIRE(equal(numbers.begin(), numbers.end(), expected.begin()));
    }

    SECTION("can avoid making copies with ref")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 };

        for(const int& item : vec)
            cout << item << " ";
        cout << endl;
    }

    SECTION("can be used with auto")
    {
        vector<string> words = { "one", "two", "three" };

        for(const auto& item : words)
        {
            cout << item << " ";
        }
        cout << endl;
    }

    // C++17
//    SECTION("in the future")
//    {
//        vector<string> words = { "one", "two", "three" };

//        for(item : words) // for(auto&& item : words)
//            cout << item << " ";
//        cout << endl;
//    }
}

TEST_CASE("native array support in C++11")
{
    int tab[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    SECTION("begin() returns pointer to first item")
    {
        auto ptr_it = begin(tab);

        REQUIRE(ptr_it == &tab[0]);
    }

    SECTION("end() returns first pointer after last item ")
    {
        auto ptr_it = end(tab);

        REQUIRE(ptr_it == tab + 10);
    }

    SECTION("array are compatibile with stl")
    {
        auto sum = accumulate(begin(tab), end(tab), 0);
    }
}

TEST_CASE("case for auto&& in range-based-for")
{
    vector<bool> bits = { 1, 0, 0, 0, 1 };

    for(auto&& bit : bits)
        bit.flip();

    for(auto it = bits.begin(); it != bits.end(); ++it)
        (*it).flip();

    for(const auto& bit : bits)
        cout << bit;
    cout << endl;
}

void legacy_code(int* tab, int size)
{

}

TEST_CASE("evil vector<bool>")
{
    vector<int> vec = {1, 2, 3};
    int* ptr_int = &vec[1];

    legacy_code(vec.data(), vec.size());

    vector<bool> evil = {1, 0, 1};
    // bool* ptr_bool = &evil[0]; // not a standard container
}

