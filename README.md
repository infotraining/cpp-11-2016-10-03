# Books

* Tour of C++ - Bjarne Stroustroup
* The C++ Standard Library: A Tutorial and Reference, Second Edition

# Video

* https://www.youtube.com/watch?v=hEx5DNLWGgA
* https://www.youtube.com/watch?v=JfmTagWcqoE
* https://www.youtube.com/watch?v=fHNmRkzxHWs
* https://www.youtube.com/watch?v=YQs6IC-vgmo