#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>
#include <random>

using namespace std;

int calculate_square(int x)
{
    cout << "Starting calc for " << x << endl;

    random_device rd{};
    mt19937_64 rnd{rd()};
    uniform_int_distribution<> distr(1000, 5000);

    this_thread::sleep_for(chrono::milliseconds(distr(rnd)));

    return x * x;
}

int main()
{
    future<int> f1 = async(launch::async, &calculate_square, 1);

    vector<future<int>> results;
    results.push_back(move(f1));

    for(int i = 2; i <= 20; ++i)
        results.push_back(async(launch::async, &calculate_square, i));

    cout << "Results: ";
    for(auto& value : results)
    {
        cout << value.get() << " ";
        cout.flush();
    }
}
