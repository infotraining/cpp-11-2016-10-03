#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <functional>
#include <set>
#include <queue>
#include <boost/core/demangle.hpp>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;


TEST_CASE("lambdas")
{
    SECTION("simple examples")
    {
        SECTION("lambda expression")
        {
            auto add = [](int a, int b) { return a + b; };

            cout << "closure type: " << typeid(add).name() << endl;

            cout << "add(1, 2) = " << add(1, 2) << endl;

            SECTION("unwraps to closure")
            {
                class MagicLambda_24637542763
                {
                public:
                    int operator()(int a, int b) const { return a + b; }
                };

                auto add = MagicLambda_24637542763();
            }
        }
    }

    SECTION("multiple returns in C++11")
    {
        auto is_even = [](int value) -> bool {
            if (value % 2 == 0)
                return true;
            else
                return false;
        };

        vector<int> vec = { 1, 2, 3, 4, 5, 6, 7 };
        vector<int> evens;

        copy_if(vec.begin(), vec.end(), back_inserter(evens), is_even);
        REQUIRE(all_of(evens.begin(), evens.end(), is_even));
    }

    SECTION("can capture variables from outer scope")
    {


        vector<int> vec = { 5, 4, 23, 32, 77, 9 };

        SECTION("capture by value")
        {
            class MagicLambda_23769286419
            {
                const int t_;
            public:
                MagicLambda_23769286419(int t) : t_{t}
                {}

                bool operator()(int x) const { return x > t_; }
            };

//            auto first_gt_threshold
//                    = find_if(vec.begin(), vec.end(),
//                              [threshold](int x) { return x > threshold; });

            int threshold = 30;

            auto pred = [threshold](auto x) { return x > threshold; }; // time of copying threshold

            threshold = 45;

            auto first_gt_threshold
                    = find_if(vec.begin(), vec.end(), pred);

            REQUIRE(*first_gt_threshold == 32);
        }

        SECTION("capture by ref")
        {
            int sum = 0;
            int factor = 2;

            class MagicLambda_23769286419
            {
                int& t_;
            public:
                MagicLambda_23769286419(int& t) : t_{t}
                {}

                void operator()(int x)  { t_ += x; }
            };

            for_each(vec.begin(), vec.end(), [=, &sum](int x) { sum += x * factor;});
            for_each(vec.begin(), vec.end(), [&](int x) { sum += x * factor;});
        }

        SECTION("lamda can be called just after creation")
        {

            string thrust = "N";

            const double thrust_value
                    = [&thrust] { if (thrust == "N") return 1.0; else return 2.133; }();

            // [](){[](){}();}();
        }

        SECTION("can be stored in auto or in std::function")
        {
            // preferred
            auto closure = []{ cout << "closure" << endl; };
            closure();

            function<void()> f = []{ cout << "closure" << endl; };
            f();
        }
    }
}

void foo(int a) { cout << "foo(" << a << ")\n"; }

class Foo
{
public:
    void operator()(int a) { cout << "Foo.operator()(" << a << ")\n"; }
};

TEST_CASE("std::function")
{
   function<void(int)> f = &foo;
   f(1);

   f = Foo();
   f(2);

   f = [](int a) { cout << "lambda: " << a << "\n"; };
   f(3);
}

auto create_generator(int seed)
{
    auto generator = [seed]() mutable { return ++seed; };
    return generator;
}

function<int()> create_generator_cpp11(int seed)
{
    auto generator = [seed]() mutable { return ++seed; };
    return generator;
}

TEST_CASE("lambdas can be returned from function")
{
    auto gen = create_generator(100);

    REQUIRE(gen() == 101);
    REQUIRE(gen() == 102);
    REQUIRE(gen() == 103);
}

TEST_CASE("lambda + decltype")
{
    int x = 10;
    int y = 20;
    int z = 5;

    auto comparer = [](int* a, int* b) { return *a < *b; };

    set<int*, decltype(comparer)> s(comparer);
    s.insert({ &x, &y, &z });

    for(const auto& ptr : s)
        cout << *ptr << " ";
    cout << endl;
}

using WorkItem = function<void()>;

class WorkQueue
{
    queue<WorkItem> q_;
public:
    void push(WorkItem item)
    {
        q_.push(item);
    }

    void run()
    {
        while (!q_.empty())
        {
            WorkItem item;
            item = q_.front();
            q_.pop();

            item();
        }
    }
};

class Printer
{
public:
    Printer() = default;
    Printer(const Printer&) = default;
    Printer(Printer&&) = default;

    Printer& operator=(const Printer&) = default;
    Printer& operator=(Printer&&) = default;

    ~Printer()
    {
        cout << "~Printer()" << endl;
    }

    void on()
    {
        cout << "Printer::on()" << endl;
    }

    void off()
    {
        cout << "Printer::off()" << endl;
    }

    void print(const string& item)
    {
        cout << "Printer::print(" << item << ")" << endl;
    }
};

TEST_CASE("WorkQueue")
{
    WorkQueue q;

    SECTION("scheduling")
    {
        shared_ptr<Printer> p = make_shared<Printer>(); // rc: 1

        q.push([p] { p->on();}); // rc: 2
        q.push([p] { p->print("text");}); // rc: 3
        q.push([p] { p->off();}); // rc: 4
    } // rc: 3

    //----

    q.run();
}

TEST_CASE("C++14 capture expressions")
{
    unique_ptr<Printer> up(new Printer());

    {
        auto l = [up = move(up)] { up->print("from lambda"); };

        REQUIRE(up.get() == nullptr);

        l();
    }
}

