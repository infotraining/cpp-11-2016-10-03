#include <iostream>

using namespace std;

void works_with_pointer(int* ptr)
{
    if (ptr != nullptr)
    {
        cout << "value: " << *ptr << endl;
    }
    else
    {
        cout << "pointer is null" << endl;
    }
}

void works_with_pointer(long value)
{
    cout << "works with int" << endl;
}

void works_with_pointer(nullptr_t) = delete;
//{
//    cout << "special version for nullptr" << endl;
//}

int main()
{
    int x = 10;
    works_with_pointer(&x);

    int* ptr = nullptr;
    works_with_pointer(ptr);

    //works_with_pointer(nullptr); // compilation error
}
