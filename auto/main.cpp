#include <iostream>
#include <typeinfo>
#include <boost/core/demangle.hpp>
#include <vector>
#include <deque>
#include <list>
#include <map>
#include <functional>
#include <boost/core/demangle.hpp>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;
using boost::core::demangle;

auto foo()  // C++14
{
    return 42.0f;
}

TEST_CASE("auto type deduction")
{
    SECTION("simple cases")
    {
        using boost::core::demangle;

        auto i = 42;  // int
        cout << demangle(typeid(i).name()) << endl;

        auto c = 't';  // char
        cout << demangle(typeid(c).name()) << endl;

        auto l = 42L; // long
        cout << demangle(typeid(l).name()) << endl;

        auto ull = 42ull; // unsigned long long
        cout << demangle(typeid(ull).name()) << endl;

        auto value = foo();
        cout << demangle(typeid(value).name()) << endl;
    }

    SECTION("most important use-case")
    {
        // iteration over container
        list<int> container = { 1, 2, 4, 5 };

        // code
        auto it = container.cbegin();
        cout << demangle(typeid(it).name()) << endl;

        for(; it != container.cend(); ++it)
            cout << *it << " ";
        cout << endl;

        std::map<std::string, std::vector<int>, std::greater<std::string>> dict
                = { { "one", { 1, 2, 3} }, { "two", {4, 5, 6, 7} } };

        auto dict_it = dict.cbegin();
        cout << demangle(typeid(dict_it).name()) << endl;

        for(; dict_it != dict.end(); ++dict_it)
        cout << dict_it->first << ": " << dict_it->second.size() << endl;
    }
}

TEST_CASE("auto type deduction explained")
{
    int data = 10;
    const int cdata = 10;
    int& ref_data = data;
    const int& cref_data = cdata;
    int arr[10];

    SECTION("for auto&")
    {
        SECTION("const and volatile are preserved")

        auto& aref1 = cdata; // const int&
        const auto& caref1 = data; // const int&
        auto* ptr5 = &cdata; // const int*

        SECTION("array doesn't decay to pointer")
        {
            auto& ref_tab = arr; // int(&ref_tab)[10]
        }
    }

    SECTION("for auto")
    {
        SECTION("refs, const and volatile are stripped")
        {
            auto ad1 = cdata; // int
            auto ad2 = ref_data; // int
            auto ad3 = cref_data; // int
        }

        SECTION("array and function decays to pointer")
        {
            auto tab1 = arr;  // int* - tab decays to pointer
            auto f1 = foo; // float(*f1)() - function decays to pointer
        }
    }
}
