#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <tuple>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& vec)
{
    auto min_it = min_element(vec.begin(), vec.end());
    auto max_it = max_element(vec.begin(), vec.end());
    double avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    return make_tuple(*min_it, *max_it, avg);
}

TEST_CASE("tuples")
{
    vector<int> vec = { 33, 5, 23, 55, 543, 23, 534 };

    tuple<int, int, double> results = calc_stats(vec);

    cout << "min: " << get<0>(results) << endl;
    cout << "max: " << get<1>(results) << endl;
    cout << "avg: " << get<2>(results) << endl;

    int min, max;
    double avg;

    //tuple<int&, int&, double&> ref_results{min, max, avg};

    tie(min, max, avg) = calc_stats(vec);

    // C++17
    //auto[min, max, avg] = calc_stats(vec);

    cout << "min: " << min << endl;
    cout << "max: " << max << endl;
    cout << "avg: " << avg << endl;

    tie(min, max, ignore) = calc_stats(vec);
}
