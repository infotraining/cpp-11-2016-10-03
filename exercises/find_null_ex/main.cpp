#include <iostream>
#include <vector>
#include <iterator>
#include <memory>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

template <typename Container>
auto find_null(Container& c) -> decltype(begin(c))
{
    auto it = begin(c);

    for(; it != end(c); ++it)
    {
        if (*it == nullptr)
            return it;
    }

    return it;
}

TEST_CASE("find_null description")
{
    SECTION("finds first null pointer in a container of raw pointers")
    {
        vector<int*> ptrs = { new int{9}, new int{10}, NULL, new int{20}, nullptr, new int{23} };

        auto first_null_appearence = find_null(ptrs);

        REQUIRE(distance(ptrs.begin(), first_null_appearence) == 2);

        for(const auto* ptr : ptrs)
            delete ptr;   
    }

    SECTION("finds first null pointer in native array of raw pointers")
    {
        int* ptrs[] = { new int{9}, new int{10}, NULL, new int{20}, nullptr, new int{23} };

        auto first_null_appearence = find_null(ptrs);

        REQUIRE(distance(begin(ptrs), first_null_appearence) == 2);

        for(const auto* ptr : ptrs)
            delete ptr;
    }

    SECTION("finds first empty shared_ptr in a container of shared_ptrs")
    {
        auto il = { make_shared<int>(10), shared_ptr<int>{}, make_shared<int>(3) };

        auto first_null_appearence = find_null(il);

        REQUIRE(distance(il.begin(), first_null_appearence) == 1);
    }

    SECTION("when all pointers are valid returns iterator which equals end()")
    {
        auto il = { make_shared<int>(10), shared_ptr<int>{new int(5)}, make_shared<int>(3) };

        REQUIRE(find_null(il) == il.end());
    }
}
