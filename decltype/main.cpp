#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <type_traits>
#include <complex>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

int foo(string);
auto foo(string) -> int;

template <typename Vector>
auto multiply(const Vector& a, const Vector& b) //-> decltype(a * b)
{
    return a * b;
}

namespace Cpp14
{
    namespace copy_returned
    {
        auto get_value(map<int, string>& dict, int key)
        {
            auto it = dict.find(key);

            if (it == dict.end())
                throw std::out_of_range("Key not found");

            return dict[key];
        }
    }

    namespace ref_returned
    {
        decltype(auto) get_value(map<int, string>& dict, int key)
        {
            auto it = dict.find(key);

            if (it == dict.end())
                throw std::out_of_range("Key not found");

            return dict[key];
        }
    }
}

TEST_CASE("decltype")
{
    SECTION("can deduce any type from expression")
    {
        int x;
        decltype(x) y;  // int y;

        string str = "text";

        auto result_type = is_same<decltype(str + "cstring"), string>::value;
        REQUIRE(result_type);

        auto result = multiply(complex<double>{1, 2}, complex<double>{2, 3});

        cout << "result of multiply: " << result << endl;
    }
}
